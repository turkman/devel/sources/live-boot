build:
	: do nothing
install:
	mkdir -p ${DESTDIR}/etc/initrd/scripts/
	mkdir -p ${DESTDIR}/etc/initrd/hooks/
	mkdir -p ${DESTDIR}/usr/libexec/
	mkdir -p ${DESTDIR}/etc/init.d/
	mkdir -p ${DESTDIR}/usr/share/initramfs-tools/hooks/
	install live-boot.script ${DESTDIR}/etc/initrd/scripts/live-boot.sh
	install live-boot.hook ${DESTDIR}/etc/initrd/hooks/live-boot.sh
	install live-config.initd ${DESTDIR}/etc/init.d/live-config
	install live-config.sh ${DESTDIR}/usr/libexec/live-config
	echo 'hooks="base eudev modules live-boot"' > ${DESTDIR}/etc/initrd/config-live.sh
	echo 'modules="most"' >> ${DESTDIR}/etc/initrd/config-live.sh

