#!/bin/sh
if command -v useradd >/dev/null ; then
    useradd -m pingu -s /bin/bash
    yes 'live' | passwd pingu
    yes 'live' | passwd root
    for grp in audio video input tty wheel users ; do
        usermod -a -G $grp pingu 2>/dev/null
    done
    # autologin settings
    sed -i "s/agetty_options=\"/& --autologin pingu /g" /etc/conf.d/agetty
fi
# hostname settings
echo "turkish" > /etc/hostname


# language settings
mkdir -p /lib64/locale
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
echo "" >> /etc/locale.gen
locale-gen
sed -i "s/C/en_US/g" /etc/profile.d/locale.sh

# rootless polkit
cat > /etc/polkit-1/rules.d/00-live.rules <<EOF
polkit.addRule(function(action, subject) {
    return polkit.Result.YES;
}
EOF

# autologin sddm
mkdir -p /etc/sddm.conf.d
cat > /etc/sddm.conf.d/00-live.conf <<EOF
[Autologin]
User=pingu
EOF

# autologin lightdm
mkdir -p /usr/share/lightdm/lightdm.conf.d/
cat > /usr/share/lightdm/lightdm.conf.d/00-live.conf <<EOF
[Seat:*]
autologin-user=pingu
autologin-user-timeout=0
EOF

# live hooks
for hook in $(ls /lib/live/ 2>/dev/null) ; do
    /bin/sh /lib/live/$hook
done

